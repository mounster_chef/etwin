# Eternal-Twin client API types

This package contains the types and interfaces defining the Eternal-Twin API.

## Actions

- `yarn build`: Compile the library
- `yarn test`: Compile the tests and run them
- `yarn lint`: Check for common errors and style issues.
- `yarn format`: Attempt to fix style issues automatically.
