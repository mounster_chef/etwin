/**
 * The client secret is a secret key used to consume the Eternal-Twin API.
 */
export type ClientSecret = string;
