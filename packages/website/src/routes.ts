/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: readonly string[] = [
  "/",
  "/legal",
  "/login",
  "/register",
  "/register/email",
  "/register/username",
  "/users/:user_id",
  "/settings"
];
